<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class User extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function posts()
    {
        return $this->belongsToMany(Post::class, 'comments', 'post_id', 'user_id')
            ->withPivot('comment');
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug_name'] = Str::slug($value);
    }

    // public function getRouteKeyName()
    // {
    //     return 'slug_name';
    // }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
}
