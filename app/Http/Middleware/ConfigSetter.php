<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDO;

class ConfigSetter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $domain = $request->domain ?? null;
        if (is_null($domain) && $this->databaseExist($domain)) {
            die('domain null');
        }

        $mysql = 'database.connections.mysql';
        //config(["$mysql.host" => env("DB_HOST_$domain", '127.0.0.1')]); //comment jika ingin sama
        //config(["$mysql.port" => env("DB_PORT_$domain", '3306')]); //comment jika ingin sama
        config(["$mysql.database" => "db_$domain"]);
        //config(["$mysql.username" => env("DB_USERNAME_$domain", 'forge')]); //comment jika ingin sama
        config(["$mysql.password" => env("DB_PASSWORD_$domain", 'forge')]);

        return $next($request);
    }

    private function databaseExist($domain)
    {
        //disini proses check ke panel apakah company exist atau tidak sebelum create database
        $companyExist = $this->checkCompay($domain);
        if ($companyExist) {
            try {
                DB::statement('CREATE DATABASE :db if Not Exists', ['db' => "db_$domain"]);
            } catch (Exception $e) {
                die('database not found');
            }
        }
    }

    private function checkCompany($domain)
    {
        //buat pengecekan company di panel
    }
}
