<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::updateOrCreate(['id' => 1, 'name' => 'Super Admin']);
        Role::updateOrCreate(['id' => 2, 'name' => 'Admin']);
        Role::updateOrCreate(['id' => 3, 'name' => 'Sales']);
        Role::updateOrCreate(['id' => 4, 'name' => 'Enginer']);
        Role::updateOrCreate(['id' => 5, 'name' => 'User']);
    }
}
