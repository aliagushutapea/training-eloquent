<?php

namespace Database\Seeders;

use App\Models\Comment;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Comment::updateOrCreate([
            'id' => 1,
            'post_id' => 1,
            'user_id' => 1,
            'comment' => 'Covid hanya ilusi'
        ]);

        Comment::updateOrCreate([
            'id' => 2,
            'post_id' => 1,
            'user_id' => 2,
            'comment' => 'Covid menjadi ajang bisnis bagi oknum pemerintah'
        ]);

        Comment::updateOrCreate([
            'id' => 3,
            'post_id' => 2,
            'user_id' => 2,
            'comment' => 'Vaksin sinovac harganya cukup mahal'
        ]);

        Comment::updateOrCreate([
            'id' => 4,
            'post_id' => 2,
            'user_id' => 3,
            'comment' => 'Ada beberapa orang yang meninggal setelah vaksin'
        ]);

        Comment::updateOrCreate([
            'id' => 5,
            'post_id' => 2,
            'user_id' => 5,
            'comment' => 'Covid tidak ditakuti di Madura'
        ]);
    }
}
