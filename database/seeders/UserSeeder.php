<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::updateOrCreate(['id' => 1, 'name' => 'Ryan']);
        User::updateOrCreate(['id' => 2, 'name' => 'Rio']);
        User::updateOrCreate(['id' => 3, 'name' => 'Rino']);
        User::updateOrCreate(['id' => 4, 'name' => 'Mailele']);
        User::updateOrCreate(['id' => 5, 'name' => 'Jasin']);
    }
}
