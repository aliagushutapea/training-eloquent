<?php

namespace Database\Seeders;

use App\Models\Post;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Post::updateOrCreate([
            'id' => 1,
            'id_publisher' => 1,
            'title' => 'Covid semakin didepan'
        ]);

        Post::updateOrCreate([
            'id' => 2,
            'id_publisher' => 1,
            'title' => 'Vaksin covid ternyata tidak 100 efektif'
        ]);
    }
}
